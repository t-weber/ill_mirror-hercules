/*******************************************************************************
* Instrument: Tutorial 1 for hercules course: Source and detectors
*
* %Identification
* Written by: Tobias Weber (tweber@ill.fr)
* Date: 28-Mar-2019
* Origin: ILL
* License: see 'LICENSE' file
* 
* %Description
*
* %End
*******************************************************************************/


DEFINE INSTRUMENT Tut1
(
	/* source wavelength */
	double src_lam = 4.5, double src_dlam = 0.5
)


DECLARE
%{
	/* Distances */
	double dist_src_det = 1.;

	/* Source */
	double src_rad = 0.01;
	double src_k = -1.;
	double src_E = -1.;

	/* Detectors */
	double mon_width = 0.25, mon_height = 0.25;


	/* ----------------------------------------------------------------------------- */
	/* Helper functions */
	double lam_to_k(double lam)
	{
		return 2.*PI / lam;
	}

	double lam_to_v(double lam)
	{
		double k = lam_to_k(lam);
		double p = HBAR * k*1e10;
		return p / MNEUTRON;
	}

	double lam_to_E(double lam)
	{
		double v = lam_to_v(lam);
		return v*v * VS2E;
	}
	/* ----------------------------------------------------------------------------- */
%}


INITIALIZE
%{
	if(src_k < 0.)
		src_k = lam_to_k(src_lam);
	if(src_E < 0.)
		src_E = lam_to_E(src_lam);

	printf("--------------------------------------------------------------------------------\n");
	printf("Source: lambda = %.4f A, k = %.4f 1/A, E = %.4f meV\n", src_lam, src_k, src_E);
	printf("--------------------------------------------------------------------------------\n");
%}


/* ----------------------------------------------------------------------------- */

TRACE

COMPONENT origin = Progress_bar()
AT (0, 0, 0) ABSOLUTE



/* ----------------------------------------------------------------------------- */
/* Source */

COMPONENT Src = Source_simple(
	radius = src_rad,
	lambda0 = src_lam, dlambda = src_dlam, gauss = 1, flux = 1e10,
	dist = dist_src_det, focus_xw = mon_width, focus_yh = mon_height)
AT (0, 0, 0) RELATIVE PREVIOUS
EXTEND
%{
	/* perfect collimation, parallel beam */
	/* vx = vy = 0; */
%}




/* ----------------------------------------------------------------------------- */
/* Detectors */

COMPONENT psdmon = PSD_monitor(
	nx = 128, ny = 128,
	filename = "psd.dat",
	xwidth = mon_width, yheight = mon_height,
	restore_neutron = 1)
AT (0, 0, dist_src_det) RELATIVE Src

COMPONENT divmon = Divergence_monitor(
	nh = 128, nv = 128,
	maxdiv_h = 3, maxdiv_v = 3,
	filename = "div.dat",
	xwidth = mon_width, yheight = mon_height,
	restore_neutron = 1)
AT (0, 0, 0) RELATIVE PREVIOUS

COMPONENT lmon = L_monitor(
	nL = 128,
	filename = "lam.dat",
	xwidth = mon_width, yheight = mon_height,
	Lmin = src_lam - src_dlam*2, Lmax = src_lam + src_dlam*2,
	restore_neutron = 1)
AT (0, 0, 0) RELATIVE PREVIOUS

COMPONENT emon = E_monitor(
	nE = 128,
	filename = "E.dat",
	xwidth = mon_width, yheight = mon_height,
	Emin = src_E - src_E*0.5, Emax = src_E + src_E*0.5,
	restore_neutron = 1)
AT (0, 0, 0) RELATIVE PREVIOUS


END
