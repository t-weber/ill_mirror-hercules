%
% preparation of the TAS lecture
% @author Tobias Weber <tweber@ill.fr>
% @date 13-jul-2018
% @license see 'LICENSE' file
%

\documentclass[english]{book}

\usepackage{amsmath}
\usepackage{tensor}
\usepackage{bm}
\usepackage{graphicx}
\usepackage{siunitx}
\usepackage{babel}

\usepackage[a4paper]{geometry}
\geometry{tmargin=2.5cm, bmargin=2.5cm, lmargin=2cm, rmargin=2cm}

\usepackage[colorlinks=true, linkcolor=black, citecolor=blue, urlcolor=blue, unicode=true]{hyperref}




\begin{document}

\title{Notes on Triple Axis Spectroscopy}
\author{T. Weber, tweber@ill.fr}
\maketitle
\tableofcontents





% ====================================================================================================================================
\chapter{Neutron Scattering}

\begin{center}
	\includegraphics[width = 0.75 \textwidth]{recip}
\end{center}


\section{Nuclear Scattering}

The  cross-sections for neutron-nuclear scattering are derived in \cite[Ch. 2 and 3]{Squires2012} of which we summarise the most important results here.
An excellent overview can also be found in \cite[Ch. 2]{Shirane2002}.

The double-differential cross-section is the number of neutrons that are scattered into a solid angle $d\Omega$ and energy range 
$\left[ E_f, E_f + dE \right]$, normalised to time $t$, Flux $\Phi$, $d\Omega$, and $dE$ \cite[p. 10]{Squires2012},
\begin{equation}
\left(\frac{d^2 \sigma}{d\Omega dE_f}\right)_{i \rightarrow f} = \frac{1}{\Phi \cdot d\Omega \cdot dE_f} \cdot R_{i \rightarrow f},
\end{equation}
where the transition rate $R_{i \rightarrow f}$ is given by Fermi's Golden Rule \cite[p. 509]{Merzbacher1998}:
\begin{equation}
	R_{i \rightarrow f} = \frac{2\pi}{\hbar} \left| \left< i | V | f \right> \right|^2 \cdot N_f,
\end{equation}
with the number of final states, $N_f$, in the range $\left[ E_f, E_f + dE \right]$.

Neutron-nuclear scattering is described using the Fermi pseudo-potential $V$ \cite[p. 15]{Squires2012}:
\begin{equation}
	V \left( \bm{r} \right) = \frac{2\pi \hbar^2}{m_n} \cdot b \cdot \delta \left( \bm{r} \right),
\end{equation}
\begin{equation}
V \left( \bm{Q} \right) = \frac{2\pi \hbar^2}{m_n} \cdot b.
\end{equation}

Summing over all available final states $\left| f \right>$ and thermally averaging over the initial states $\left| i \right>$, 
results in \cite[p. 20]{Squires2012}:
\begin{equation}
	\boxed{ \frac{d^2 \sigma}{d\Omega dE_f} = \frac{1}{2\pi \hbar} \frac{k_f}{k_i} \cdot
		\sum_{kl}{b_k b_l \int{dt \cdot 
			\exp{ \left( -\imath \omega t \right)} \cdot
			\left< 
			\exp{\left( -\imath \bm{Q} \cdot \bm{\hat{R}_k} \left(t=0 \right) \right)}
			\exp{\left(\imath \bm{Q} \cdot \bm{\hat{R}_l} \left(t \right) \right)} 
			\right>_{\mathrm{therm.}} }}, }
\end{equation}
with the Heisenberg operators $\bm{\hat{R}_l} \left(t \right)$.
The double-differential cross-section is also often expressed as the $k_i$- and $k_f$-independent scattering function $S\left(\bm{Q}, \omega \right)$ 
\cite[p. 21]{Shirane2002}:
\begin{equation}
	S\left(\bm{Q}, \omega \right) = \frac{d^2 \sigma}{d\Omega dE_f} \cdot \frac{k_i}{k_f b^2 N},
\end{equation}
for which the detailed-balance relation holds \cite[p. 26]{Shirane2002}:
\begin{equation}
	S\left(\bm{Q}, \omega \right) = \exp \left( \frac{\hbar \omega}{k_B T} \right) \cdot S \left( -\bm{Q}, -\omega \right).
\end{equation}


Averaging the scattering lengths $b_k b_l$ into an effective $b$ leads to two distinct contributions to the scattering
cross section \cite[p. 22]{Squires2012}:
\begin{equation} \begin{split}
	\frac{d^2 \sigma}{d\Omega dE_f} & = 
		\frac{1}{2\pi \hbar} \frac{k_f}{k_i} \cdot
		\left< b \right> ^2 \cdot
		\sum_{kl}{ \int{dt \cdot 
			\exp{ \left( -\imath \omega t \right)} \cdot
			\left< 
				\exp{\left( -\imath \bm{Q} \cdot \bm{\hat{R}_k} \left(t=0 \right) \right)}
				\exp{\left(\imath \bm{Q} \cdot \bm{\hat{R}_l} \left(t \right) \right)} 
			\right>_{\mathrm{therm.}} }} \\
		& + \frac{1}{2\pi \hbar} \frac{k_f}{k_i} \cdot
		\underbrace{\left( \left< b^2 \right> - \left< b \right>^2 \right)}_{\equiv b_{inc}^2} \cdot
		\sum_{k}{ \int{dt \cdot 
			\exp{ \left( -\imath \omega t \right)} \cdot
			\left< 
				\exp{\left( -\imath \bm{Q} \cdot \bm{\hat{R}_k} \left(t=0 \right) \right)}
				\exp{\left(\imath \bm{Q} \cdot \bm{\hat{R}_k} \left(t \right) \right)} 
			\right>_{\mathrm{therm.}} }}.
\end{split} \end{equation}
The first term of the sum is the coherent contribution (i.e. collective phenomena: phonons, Bragg peaks), 
the second one the incoherent contribution (spin- and nuclear-incoherent peaks, diffuse scattering).



\subsection*{Phonons}

If $\bm{r_i}$ points to the time-independent rest position of an atom $i$ and $\bm{u_i}$ is its time-dependent displacement, 
with $\bm{R_i} = \bm{r_i} + \bm{u_i}$, the integral depends only on the displacements:

\begin{equation}
	\frac{d^2 \sigma}{d\Omega dE_f} = 
		\frac{1}{2\pi \hbar} \frac{k_f}{k_i} \cdot
		\sum_{kl}{
		b_k b_l \cdot
		\exp{\left[ - \imath \bm{Q} \cdot \left(\bm{\hat{r}_k} - \bm{\hat{r}_l} \right) \right]} \cdot
		\int{dt \cdot 
			\exp{ \left( -\imath \omega t \right)} \cdot
			\left< 
				\exp{\left( -\imath \bm{Q} \cdot \bm{\hat{u}_k} \left(t=0 \right) \right)}
				\exp{\left(\imath \bm{Q} \cdot \bm{\hat{u}_l} \left(t \right) \right)} 
			\right>_{\mathrm{therm.}} }}.
\end{equation}


A derivation of the atomic displacement operator $\bm{\hat{u}_i}$ is given in \cite[Appendix G]{Squires2012}, it is assumed 
to be harmonic. A Taylor-expansion of the $Q$-dependent exponential terms in the integral leads to 0-phonon, 1-phonon, 2-phonon, ... processes.

The results for the 0-phonon (elastic) process leads to Bragg peaks whose intensities are described using the nuclear 
structure factor, $I = |F_N|^2$ as well as an incoherent background \cite[p. 37, p. 56]{Squires2012}:

\begin{equation} \begin{split}
	\left( \frac{d \sigma}{d\Omega} \right)_{0-\mathrm{phonon}} & = 
	\left( 2\pi \right)^3 \frac{N_{uc}}{V_{uc}} \cdot 
	\underbrace{\sum_{\bm{G}}{ \delta \left( \bm{Q} - \bm{G} \right) }}_{\mathrm{Bragg\ condition}} \cdot
	\left| \underbrace{\sum_{i \in \mathrm{UC\ at.}}{ \left< b_i \right> \exp{\left( \imath \bm{Q} \cdot \bm{r}_i^{uc} \right)} 
		\cdot \exp \left( -W_i \right)
		}}_{\mathrm{structure\ factor}\ F_N} \right|^2 \\
	& + \underbrace{N_{uc} \cdot \sum_{i \in \mathrm{UC\ at.}} { \left( \left< b_i^2 \right> - \left< b_i \right>^2 \right) \cdot 
		\exp \left( -2W_i \right)
	}}_{\mathrm{incoherent\ part}}.
\end{split} \end{equation}



The 1-phonon process is given as \cite[p. 46, p. 56]{Squires2012}: 

\begin{equation} \begin{split}
	\left( \frac{d^2 \sigma}{d\Omega dE_f} \right)_{1-\mathrm{phonon}} & = 
		\frac{1}{2} \frac{\left( 2\pi \right)^3}{V_{uc}} \frac{k_f}{k_i} \cdot
		\sum_{\bm{G} \bm{q} j} \frac{1}{\omega_{j}\left( \bm{q} \right)} \cdot \\
		&	\hspace{0.5cm} \cdot
		\left[ 
			\underbrace{\left< n_{\bm{q} j} + 1 \right> \cdot \delta \left( \bm{Q} - \bm{G} - \bm{q} \right) \cdot \delta \left( \omega - \omega_{j}\left( \bm{q} \right) \right)}_{\mathrm{phonon\ creation}} \ + \ 
			\underbrace{\left< n_{\bm{q} j} \right> \cdot \delta \left( \bm{Q} - \bm{G} + \bm{q} \right) \cdot \delta \left( \omega + \omega_{j}\left( \bm{q} \right) \right)}_{\mathrm{phonon\ annihilation}}
		\right] \cdot \\
		&	\hspace{0.5cm} \cdot \left| \sum_{i \in \mathrm{UC\ at.}}{ 
				\frac{\left< b_i \right>}{m_i} \cdot
				\bm{Q} \cdot \bm{e}_{i \bm{q} j}  \cdot
				\exp \left( \imath \bm{Q} \cdot \bm{r}_i^{uc} \right) \cdot
				\exp \left( -W_i \right) }
			\right|^2	\\
		& + \frac{k_f}{k_i} \cdot
				\sum_{\bm{q} j} \frac{1}{\omega_{j}\left( \bm{q} \right)} \cdot \\
		& \hspace{0.5cm}	\cdot \left[ 
					\underbrace{\left< n_{\bm{q} j} + 1 \right> \cdot \delta \left( \omega - \omega_{j}\left( \bm{q} \right) \right)}_{\mathrm{phonon\ creation}} \ + \ 
					\underbrace{\left< n_{\bm{q} j} \right> \cdot \delta \left( \omega + \omega_{j}\left( \bm{q} \right) \right)}_{\mathrm{phonon\ annihilation}}
				\right] \cdot  \\
		& \hspace{0.5cm}	\cdot \sum_{i \in \mathrm{UC\ at.}} \left( \left< b_i^2 \right> - \left< b_i \right>^2 \right) \cdot 
			\left| \bm{Q} \cdot \bm{e}_{i \bm{q} j} \right|^2 \cdot \exp \left( -2W_i \right),
\end{split} \end{equation}

with the reduced momentum transfer $\bm{q}$, the  polarisation direction $\bm{e}_{i \bm{q} j}$, the index of an atom in 
the unit cell $i$ and the polarisation index $j$. 
Note that the incoherent part has no restriction to $\bm{Q} = \bm{G} + \bm{q}$, it does therefore not measure the dispersion, 
but the density of states $D\left( \omega \right)$.



% ------------------------------------------------------------------------------------------------------------------------------------


\section{Magnetic Scattering}

For magnetic scattering the potential $V$ is not a simple delta-function as with nuclear scattering, but comprises two factors due to
the interaction of the neutron magnetic moment with the electron spin and orbital motion \cite[p. 130]{Squires2012}:

\begin{equation}
	V = -\bm{\mu}_n \cdot \bm{B}_e = -\bm{\mu}_n \cdot \frac{\mu_0}{4\pi} \cdot
		\left[ \overbrace{\bm{\nabla} \times \left( \frac{\bm{\mu}_e \times \bm{r}}{r^3} \right)}^{\mathrm{due\ to\ e^{-}\ spin}} 
		\ \underbrace{-\  2 \cdot \frac{\mu_B}{\hbar} \cdot \frac{\bm{p}_e \times \bm{r}}{r^3}}_{\mathrm{due\ to\ e^{-}\ motion}} \right].
\end{equation}


Considering only coherent, spin-only scattering, the resulting cross-section can be written as \cite[p. 38]{Shirane2002}:
\begin{equation} \begin{split}
	\left( \frac{d^2 \sigma}{d\Omega dE_f} \right)_{\mathrm{coh.}} & = 
		\frac{N}{2 \pi \hbar} \left( \frac{\gamma}{2} \frac{e^2}{m_e c^2} \right)^2 g^2 f^2\left( \bm{Q} \right)
		\frac{k_f}{k_i}  \cdot  \exp \left( -2 W \right) \cdot \\
		& \cdot \hspace{0.5cm} \sum_{\alpha \beta} \left( \delta_{\alpha \beta} - \frac{Q_{\alpha} Q_{\beta}}{Q^2} \right) \cdot \\
		& \cdot \hspace{0.5cm} \int dt \sum_{i \in \mathrm{atoms}} \exp{\left( \imath \bm{Q} \cdot \bm{r}_i \right)} \cdot
			\left< M_{0, \alpha}\left( t=0 \right) M_{i, \beta}\left( t \right) \right>_{\mathrm{therm.}} \cdot
			\exp\left( -\imath \omega t \right),
\end{split} \end{equation}
where $f(\bm{Q})$ is the magnetic form factor, which is the Fourier-transform of the spin density of the unpaired electrons
(normalized to the unpaired electron number). 
$M_{i, \alpha} (t)$ are the $\alpha$-components ($\alpha=1,2,3$) of the magnetisation (here: only due to spin) for (magnetic) 
atom $i$ at time $t$. 
The two Fourier transformations in the last line form the absolute square of the magnetic interaction vector 
$\bm{M}(\bm{Q}, \omega)$. 
Line two is an orthogonal projector which causes only the components perpendicular to $\bm{Q}$ to be registered
by neutron scattering:
\begin{equation}
	\left| M_{\perp}\left(\bm{Q}\right) \right> \ = \ 
		\underbrace{ \left( 1 -
			\frac{ \left| Q \left> \right< Q \right| }{  \left< Q | Q \right> } \right)
		}_{\mathrm{ortho.\ projector}}
				 \ \cdot \ \left| M \left(\bm{Q}\right) \right>.
\end{equation}

% ====================================================================================================================================






% ====================================================================================================================================
\chapter{Crystal Coordinates and TAS Angles}


% ------------------------------------------------------------------------------------------------------------------------------------
\section{Fractional Coordinates}

\begin{center}
	\includegraphics[width = 0.2 \textwidth]{cell}
\end{center}

\subsection*{Basic Properties}

From the cosine theorem we get:
\begin{equation} \left< a | b \right > = ab \cos \gamma, \label{ab} \end{equation}
\begin{equation} \left< a | c \right > = ac \cos \beta, \label{ac} \end{equation}
\begin{equation} \left< b | c \right > = bc \cos \alpha. \label{bc} \end{equation}


\subsection*{Basis Vectors}

We first choose $\left| a \right>$ along $x$,
\begin{equation} \boxed{ \left| a \right> = \left( \begin{array}{c} a_1 = a \\ 0 \\ 0 \end{array} \right), } \label{avec} \end{equation}
$\left| b \right>$ in the $xy$ plane,
\begin{equation} \left| b \right> = \left( \begin{array}{c} b_1 \\ b_2 \\ 0 \end{array} \right), \end{equation}
and $\left| c \right>$ in general:
\begin{equation} \left| c \right> = \left( \begin{array}{c} c_1 \\ c_2 \\ c_3 \end{array} \right). \end{equation}


Inserting $\left| a \right>$ and $\left| b \right>$ into Eq. \ref{ab} gives:

\begin{equation} \left< a | b \right > = a_1 b_1 = ab \cos \gamma, \end{equation}
\begin{equation} b_1 = b \cos \gamma. \end{equation}


Using the cross product between $\left| a \right>$ and $\left| b \right>$, we get:
\begin{equation} \left\Vert \left| a \right> \times \left| b \right> \right\Vert =
	\left\Vert \left( \begin{array}{c} 0 \\ 0 \\ a_1 b_2 \end{array} \right) \right\Vert =
	ab \sin \gamma, \label{crossab}
\end{equation}
\begin{equation} b_2 = b \sin \gamma, \end{equation}
\begin{equation} \boxed{ \left| b \right> = \left( \begin{array}{c} b \cos \gamma \\ b \sin \gamma \\ 0 \end{array} \right). } \label{bvec} \end{equation}


Inserting $\left| a \right>$ and $\left| c \right>$ into Eq. \ref{ac} gives:

\begin{equation} \left< a | c \right > = a_1 c_1 = ac \cos \beta, \end{equation}
\begin{equation} c_1 = c \cos \beta. \end{equation}



Inserting $\left| b \right>$ and $\left| c \right>$ into Eq. \ref{bc} gives:

\begin{equation} \left< b | c \right > = b_1 c_1 + b_2 c_2 = bc \cos \alpha, \end{equation}
\begin{equation} b \cos \gamma \cdot c \cos \beta + b \sin \gamma \cdot c_2 = bc \cos \alpha, \end{equation}
\begin{equation} c_2 = \frac{c \cos \alpha - c \cos \gamma \cos \beta}{\sin \gamma}. \end{equation}



The last component, $c_3$, can be obtained from the vector length normalisation, $ \left< c | c \right> = c^2 $:
\begin{equation} \left< c | c \right > = c_1^2 + c_2^2 + c_3^2 = c^2, \end{equation}
\begin{equation} c_3^2 = c^2 - c_1^2 - c_2^2, \end{equation}
\begin{equation} c_3^2 = c^2 \left[1 - \cos^2 \beta - \left(\frac{\cos \alpha - \cos \gamma \cos \beta}{\sin \gamma} \right)^2 \right], \end{equation}
\begin{equation} \boxed{ \left| c \right> = \left( \begin{array}{c}
	c \cdot \cos \beta \\
	c \cdot \frac{\cos \alpha - \cos \gamma \cos \beta}{\sin \gamma} \\
	c \cdot \sqrt{ 1 - \cos^2 \beta - \left(\frac{\cos \alpha - \cos \gamma \cos \beta}{\sin \gamma} \right)^2 }
\end{array} \right). } \label{avec} \end{equation}



The crystallographic $A$ matrix, which transforms real-space fractional to lab coordinates (\AA), is formed with the basis vectors in its columns:
\begin{equation}
	A = \left(
		\begin{array}{ccc}
			\left| a \right> & \left| b \right> & \left| c \right>
		\end{array}
	\right).
\end{equation}
The $B$ matrix, which transforms reciprocal-space relative lattice units (rlu) to lab coordinates (1/\AA), is:
\begin{equation} B = 2 \pi A^{-t}, \end{equation}
where $-t$ denotes the transposed inverse.
The metric tensor corresponding to the coordinate system defined by the $B$ matrix is:
\begin{equation} \left(g_{ij}\right) = \left<\bm{b_i} | \bm{b_j} \right> = B^T B, \end{equation}
where the reciprocal basis vectors $\left| \bm{b_i} \right>$ form the columns of $B$.


\subsection*{Example: Lengths and Angles in the Reciprocal Lattice}
Having a metric makes it straightforward to calculate lengths and angles.
The length of a reciprocal lattice vector $\left| G \right>$ seen from the lab system is (in 1/\AA{} units):
\begin{equation}
	\left\Vert \left< G | G \right> \right\Vert = \sqrt{\left< G | G \right>} = \sqrt{G_i G^j} = \sqrt{g_{ij} G^i G^j}.
\end{equation}
The angle $\theta$ between two Bragg peaks $\left| G \right>$ and $\left| H \right>$ is given by their dot product:
\begin{equation}
	\frac{\left< G | H \right>}{\left\Vert \left< G | G \right> \right\Vert \cdot \left\Vert \left< H | H \right> \right\Vert} = \cos \theta,
\end{equation}
%\begin{equation}
%	\frac{G_i H^j }{\left\Vert \left< G | G \right> \right\Vert \cdot \left\Vert \left< H | H \right> \right\Vert} = \cos \theta,
%\end{equation}
\begin{equation}
	\frac{g_{ij} G^i H^j }{\sqrt{g_{ij} G^i G^j} \sqrt{g_{ij} H^i H^j}} = \cos \theta.
\end{equation}


% ------------------------------------------------------------------------------------------------------------------------------------




% ------------------------------------------------------------------------------------------------------------------------------------
\section{TAS Angles and Scattering Triangle}
\begin{figure}
\begin{center}
	\includegraphics[width = 0.5 \textwidth]{tas}
	\hspace{1.5cm}
	\includegraphics[trim=0 -2cm 0 0, width=0.25\textwidth]{triangle}
\end{center}
\caption{Triple-axis layout and scattering triangle.}
\end{figure}

\subsection*{Monochromator Angles $a_1$, $a_2$ and Analyser Angles $a_5$, $a_6$}

The monochromator (and analyser) angles follow directly from Bragg's equation:
\begin{equation} 2 d_{m,a}\sin a_{1,5} = n \lambda_{i,f}, \end{equation}
\begin{equation} 2 k_{i,f} \sin a_{1,5} = 2 \pi n / d_{m,a}, \end{equation}
\begin{equation} \boxed{ a_{1,5} = \arcsin \left( \frac{\pi n}{d_{m,a} \cdot k_{i,f}} \right). } \end{equation}

Fulfilling the Bragg condition, the angles $a_2$ and $a_6$ are simply: $a_{2,6} = 2 \cdot a_{1,5}.$


\subsection*{Scattering Angle $a_4$}

\begin{equation} \left| Q \right> = \left| k_i \right> - \left| k_f \right> \\ \end{equation}
\begin{equation} \left< Q | Q \right> = \left( \left< k_i \right| - \left< k_f \right| \right) \cdot \left( \left| k_i \right> - \left| k_f \right> \right) \end{equation}
\begin{equation} \left< Q | Q \right> = \left< k_i | k_i \right> + \left< k_f | k_f \right> - 2 \left< k_i | k_f \right> \end{equation}
\begin{equation} Q^2 = k_i^2 + k_f^2 - 2 k_i k_f \cos a_4 \end{equation}
\begin{equation} \boxed{ a_4 = \sigma_s \cdot \arccos \left( \frac{k_i^2 + k_f^2 - Q^2}{2 k_i k_f} \right) } \end{equation}

The sign of $a_4$ is given by the sample scattering sense $\sigma_s = \pm 1$.



\subsection*{Rocking Angle $a_3$}

\begin{equation} \boxed{ a_3 = 180^{\circ} - \left( \psi + \xi \right) } \end{equation}


\subsubsection*{Angle $\psi$}
Angle $\psi$ between $\left| k_i \right>$ and $\left| Q \right>$, in units of \AA{}$^{-1}$, as before:
\begin{equation} \left| k_f \right> = \left| k_i \right> - \left| Q \right> \end{equation}
\begin{equation} \left< k_f | k_f \right> = \left( \left< k_i \right| - \left< Q \right| \right) \cdot \left( \left| k_i \right> - \left| Q \right> \right) \end{equation}
\begin{equation} \left< k_f | k_f \right> = \left< k_i | k_i \right> + \left< Q | Q \right> - 2 \left< k_i | Q \right> \end{equation}
\begin{equation} k_f^2 = k_i^2 + Q^2 - 2 k_i Q \cos \psi \end{equation}
\begin{equation} \boxed{ \psi = \sigma_s \cdot \arccos \left( \frac{k_i^2 + Q^2 - k_f^2}{2 k_i Q} \right) } \end{equation}


\subsubsection*{Angle $\xi$}
Angle $\xi$ between $\left| Q \right>$ and orientation vector $\left| a \right>$ (i.e. $ax$, $ay$, $az$), in units of rlu:
\begin{equation} \xi = \sigma_{\mathrm{side}} \cdot \arccos \left( \frac{ \left< Q | a \right> }{ \sqrt{\left< Q | Q \right>} \sqrt{\left< a | a \right>} } \right) \end{equation}
\begin{equation} \boxed{ \xi = \sigma_{\mathrm{side}} \cdot \arccos \left( \frac{ Q^i g_{ij} a^j }{ \sqrt{Q^i g_{ij} Q^j} \sqrt{a^i g_{ij} a^j} } \right) } \end{equation}

The sign, $\sigma_{\mathrm{side}}$, of $\xi$ depends on which side of the orientation vector $\left| a \right>$ the scattering vector $\left| Q \right>$ 
is located. The sign can be found by calculating the (covariant) cross product of $\left| a \right>$ and $\left| Q \right>$ to give an out-of-plane vector 
which can be compared with the given scattering plane up vector.


\paragraph*{Special case}
Special case for cubic crystals, $g_{ij} = \delta_{ij} \cdot \left( 2\pi / a \right)^2$:
\begin{equation} \xi = \sigma_{\mathrm{side}} \cdot \arccos \left( \frac{ Q_i a^i }{ \sqrt{Q_i Q^i} \sqrt{a_i a^i} } \right) \end{equation}
% ------------------------------------------------------------------------------------------------------------------------------------
% ====================================================================================================================================





\bibliographystyle{alpha}
\bibliography{\jobname.bib}

\end{document}
